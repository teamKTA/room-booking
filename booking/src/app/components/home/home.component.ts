import { Component, OnInit, OnDestroy } from '@angular/core';
import { FloorService } from '../../services/floor.service';
import { RequestService } from '../../services/request.service';
import { CommonService } from '../../services/common.service';
import {
  FormControl,
  Validators,
  FormGroup,
  AbstractControl
} from '@angular/forms';
import { MatDialog } from '@angular/material';
import { BookingInformationComponent } from '../../dialogs/booking-information/booking-information.component';
import { Subscription } from 'rxjs';
import { RequestModel } from '../../models/request';
import { RequestStatus } from '../../enums/constants';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../../app.component.scss'],
  providers: [FloorService, RequestService]
})
export class HomeComponent implements OnInit, OnDestroy {
  homeFG: FormGroup;
  floors = [];
  requests: RequestModel[] = [];
  fromTimeFC = new FormControl(8, this.validateFromTime);
  toTimeFC = new FormControl(9, this.validateToTime.bind(this));
  dateSelectedFC = new FormControl(new Date(), Validators.required);
  dateFormated: any;
  floorNameFC = new FormControl('');
  roomNameFC = new FormControl('');
  fromTimes = [];
  toTimes = [];
  example = 0;
  subscription: Subscription;
  constructor(
    private floorService: FloorService,
    private requestService: RequestService,
    private commonService: CommonService,
    public dialog: MatDialog,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.dateFormated = this.commonService.transformDate(new Date());
    let time = 8;
    while (time < 21) {
      this.fromTimes.push(time);
      time++;
    }
    time = 9;
    while (time < 22) {
      this.toTimes.push(time);
      time++;
    }
    this.homeFG = new FormGroup({});
    this.addFormControls();
    this.subscription = this.floorService.itemRef
      .valueChanges()
      .subscribe(res => {
        if (res != null) {
          res.forEach(floor => {
            const floorName = floor.FloorName;
            const rooms = floor.Rooms;
            const list = Object.keys(rooms).map(modeValue => {
              return { roomID: modeValue, roomName: rooms[modeValue] };
            });
            if (list) {
              this.floors.push({ floorName: floorName, rooms: list });
            }
          });
        }
      });
    this.subscription.add(
      this.requestService.itemRef
        .valueChanges()
        .subscribe((res: RequestModel[]) => {
          if (res) {
            this.requests = res;
          }
        })
    );
    this.subscription.add(
      this.fromTimeFC.valueChanges.subscribe(res => {
        if (res && res >= this.toTimeFC.value) {
          this.toTimeFC.setValue(res + 1);
        }
      })
    );
    this.subscription.add(
      this.dateSelectedFC.valueChanges.subscribe(res => {
        if (res) {
          this.dateFormated = this.commonService.transformDate(res);
        }
      })
    );
  }

  addFormControls() {
    this.homeFG.addControl('dateSelected', this.dateSelectedFC);
    this.homeFG.addControl('fromTime', this.fromTimeFC);
    this.homeFG.addControl('toTime', this.toTimeFC);
    this.homeFG.addControl('floorName', this.floorNameFC);
    this.homeFG.addControl('roomName', this.roomNameFC);
  }

  ngOnDestroy() {
    this.floorService = null;
    this.subscription.unsubscribe();
    this.homeFG = null;
    this.floors = null;
    this.fromTimeFC = null;
    this.toTimeFC = null;
    this.dateSelectedFC = null;
    this.floorNameFC = null;
    this.roomNameFC = null;
    this.fromTimes = null;
    this.toTimes = null;
    this.example = null;
  }

  validateFromTime(control: AbstractControl) {
    if (!control.value) {
      return {
        invalidFromTime: 'Vui lòng chọn giờ bắt đầu'
      };
    }
  }

  validateToTime(control: AbstractControl) {
    if (!control.value) {
      return {
        invalidToTime: 'Vui lòng chọn giờ kết thúc'
      };
    }

    if (control.value <= this.fromTimeFC.value) {
      return {
        invalidToTime: 'Giờ kết thúc phải sau giờ bắt đầu'
      };
    }
    return null;
  }

  isBooked(floorName: string, roomName: string) {
    for (let i = 0; i < this.requests.length; i++) {
      if (
        this.requests[i].roomName === roomName &&
        this.requests[i].floorName === floorName &&
        this.requests[i].dateSelected === this.dateFormated &&
        this.requests[i].fromTime === this.fromTimeFC.value &&
        this.requests[i].toTime === this.toTimeFC.value &&
        this.requests[i].status !== RequestStatus.Approved
      ) {
        return true;
      }
    }
    return false;
  }

  onClickOpenDialog(floorName: string, roomName: string) {
    if (this.homeFG.invalid === true) {
      return;
    }
    this.floorNameFC.setValue(floorName);
    this.roomNameFC.setValue(roomName);
    const dialogRef = this.dialog.open(BookingInformationComponent, {
      disableClose: true,
      data: {
        information: Object.assign(this.homeFG.value)
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        const dataInput: RequestModel = Object.assign(this.homeFG.value, res);
        dataInput.dateSelected = this.dateFormated;
        dataInput.status = RequestStatus.Pending;
        dataInput.timeMakeRequest = new Date().toISOString();
        dataInput.id = (this.requests.length - 1).toString();
        dataInput.userName = this.userService.user.userName;
        // const date = new Date(new Date().toISOString());
        if (this.commonService.recordExisted(this.requests, dataInput)) {
          // alert existed record
        } else {
          this.requestService.addItem(
            dataInput,
            this.requests.length.toString()
          );
        }
      }
    });
  }
}
