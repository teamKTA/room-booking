import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {
  MatTableDataSource,
  MatSort,
  MatSnackBar,
  MatDialog
} from '@angular/material';
import { RequestModel } from '../../models/request';
import { RequestService } from '../../services/request.service';
import { Subscription } from 'rxjs';
import { RequestStatus } from '../../enums/constants';
import { RejectRequestComponent } from '../../dialogs/reject-request/reject-request.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['../../app.component.scss'],
  providers: [RequestService]
})
export class AdminComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort)
  sort: MatSort;
  dataSource;
  rowSelected = new RequestModel();
  listData: RequestModel[] = [];
  subscription: Subscription;
  isdataloaded = false;
  isDisable = true;
  displayedColumns = [
    'dateSelected',
    'location',
    'time',
    'personName',
    'status',
    'star'
  ];

  constructor(
    private requestService: RequestService,
    public dialog: MatDialog
  ) {}
  ngOnInit() {
    this.subscription = this.requestService.itemRef.valueChanges().subscribe(
      res => {
        if (res) {
          this.listData = res;
          this.dataSource = new MatTableDataSource(this.listData);
          this.dataSource.sort = this.sort;
        }
        this.isdataloaded = true;
      },
      err => {
        this.isdataloaded = true;
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onClickInfo(element: RequestModel) {
    this.rowSelected = element;
    this.isDisable = false;
  }

  onClickOk() {
    this.rowSelected.status = RequestStatus.Approved;
    this.requestService.updateItem(this.rowSelected, this.rowSelected.id);
    // mail to user approved
    // delete all the same request
  }

  onClickCancel() {
    const dialogRef = this.dialog.open(RejectRequestComponent, {
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(res => {
      // mail to user rejected
    });
  }
}
