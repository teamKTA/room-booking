import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../models/user';
import { Subscription } from 'rxjs';
import { UserService } from '../../services/user.service';
import { CommonService } from '../../services/common.service';
import { InformationMessageComponent } from '../../dialogs/information-message/information-message.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['../../app.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerFG: FormGroup;
  users: User[] = [];
  userNameFC = new FormControl('', this.validateUserName.bind(this));
  passwordFC = new FormControl('', this.validatePassword.bind(this));
  telFC = new FormControl('');
  emailFC = new FormControl('', this.validateEmail.bind(this));
  subscription: Subscription;
  constructor(
    public route: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnInit() {
    this.registerFG = new FormGroup({});
    this.addFormControls();
    this.subscription = this.userService.itemRef
      .valueChanges()
      .subscribe((res: User[]) => {
        if (res) {
          this.users = res;
        }
      });
  }

  validateUserName(control: AbstractControl) {
    if (control.value === '') {
      return { invalidUserName: 'Vui lòng điền User Name' };
    }

    const listUser = this.users.filter(x => x.userName === control.value);
    if (listUser.length > 0) {
      return { invalidUserName: 'User đã tồn tại' };
    }

    return null;
  }

  validatePassword(control: AbstractControl) {
    if (control.value === '') {
      return { invalidPassword: 'Vui lòng điền Password' };
    }
    return null;
  }

  validateEmail(control: AbstractControl) {
    if (control.value === '') {
      return { invalidEmail: 'Vui lòng điền Email' };
    }

    const regexp = new RegExp(
      // tslint:disable-next-line:max-line-length
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (!regexp.test(control.value.toLowerCase())) {
      return { invalidEmail: 'Email không hợp lệ' };
    }

    const listEmail = this.users.filter(
      x => x.email.toLowerCase() === control.value.toLowerCase()
    );
    if (listEmail.length > 0) {
      return { invalidEmail: 'Email đã tồn tại' };
    }

    return null;
  }

  addFormControls() {
    this.registerFG.addControl('userName', this.userNameFC);
    this.registerFG.addControl('passWord', this.passwordFC);
    this.registerFG.addControl('email', this.emailFC);
    this.registerFG.addControl('tel', this.telFC);
  }

  onClickRegister() {
    if (this.registerFG.invalid === false) {
      const newUser: User = Object.assign({}, this.registerFG.value);
      this.userService.addItem(newUser, this.users.length.toString());
      const dialogRef = this.dialog.open(InformationMessageComponent, {
        disableClose: true,
        data: {
          information: 'Đăng kí thành công'
        }
      });

      dialogRef.afterClosed().subscribe(res => {
        this.route.navigate(['/login']);
      });
    }
  }

  onClickCancel() {
    this.route.navigate(['/login']);
  }
}
