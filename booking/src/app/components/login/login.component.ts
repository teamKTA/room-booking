import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';
import { MatDialog } from '@angular/material';
import { InformationMessageComponent } from '../../dialogs/information-message/information-message.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../app.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  logInFG: FormGroup;
  userNameLogInFC = new FormControl('', Validators.required);
  passwordLoginFC = new FormControl('', Validators.required);
  errorMessage = '';
  subscription: Subscription;
  users: User[] = [];

  constructor(
    public route: Router,
    private userService: UserService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.logInFG = new FormGroup({});
    this.addFormControls();

    this.subscription = this.userService.itemRef
      .valueChanges()
      .subscribe((res: User[]) => {
        if (res) {
          this.users = res;
        }
      });
  }

  ngOnDestroy() {}

  addFormControls() {
    this.logInFG.addControl('userNameLogIn', this.userNameLogInFC);
    this.logInFG.addControl('passwordLogin', this.passwordLoginFC);
  }

  onClickLogIn() {
    const username = this.userNameLogInFC.value.toLowerCase();
    const password = this.passwordLoginFC.value.toLowerCase();
    const listUser = this.users.filter(
      x => x.userName.toLowerCase() === username && x.passWord === password
    );
    if (listUser.length > 0) {
      const dialogRef = this.dialog.open(InformationMessageComponent, {
        disableClose: true,
        data: {
          information: 'Đăng nhập thành công'
        }
      });
      dialogRef.afterClosed().subscribe(res => {
        this.userService.user = listUser[0];
        this.route.navigate(['/home']);
      });
    } else {
      this.dialog.open(InformationMessageComponent, {
        disableClose: true,
        data: {
          information: 'Đăng nhập thất bại! Xin thử lại.'
        }
      });
    }
  }

  onClickRegister() {
    this.route.navigate(['/register']);
  }

  onClickForgotPass() {
    this.route.navigate(['/forgotPass']);
  }
}
