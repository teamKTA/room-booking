export class RequestModel {
  id: string;
  dateSelected: Date;
  description: string;
  floorName: string;
  roomName: string;
  fromTime: number;
  toTime: number;
  purpose: string;
  personName: string;
  status: string;
  timeMakeRequest: string;
  userName: string;
}
