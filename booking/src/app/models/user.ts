export class User {
  id: string;
  userName: string;
  passWord: string;
  email: string;
  tel?: number;

  constructor(
    id: string,
    userName: string,
    passWord: string,
    email: string,
    tel: number
  ) {
    this.id = id;
    this.userName = userName;
    this.passWord = passWord;
    this.email = email;
    this.tel = tel;
  }
}
