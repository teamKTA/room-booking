import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { HomeComponent } from './components/home/home.component';
import { MaterialModule } from './material.module';
import { RouterModule, Routes } from '@angular/router';
import { SidenavComponent } from './components/navigation/sidenav/sidenav.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { CommonService } from './services/common.service';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { BookingInformationComponent } from './dialogs/booking-information/booking-information.component';
import { InformationMessageComponent } from './dialogs/information-message/information-message.component';
import { RejectRequestComponent } from './dialogs/reject-request/reject-request.component';
import { DatePipe } from '@angular/common';
import { RegisterComponent } from './components/register/register.component';
import { UserService } from './services/user.service';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'admin',
    component: AdminComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [
    RegisterComponent,
    BookingInformationComponent,
    InformationMessageComponent,
    RejectRequestComponent,
    AppComponent,
    LoginComponent,
    AdminComponent,
    HomeComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyCu3XNtKyeM1LOmw579YH1f5mK6J8NDkSY',
      authDomain: 'bookingapp-7af2a.firebaseapp.com',
      databaseURL: 'https://bookingapp-7af2a.firebaseio.com',
      projectId: 'bookingapp-7af2a',
      storageBucket: 'bookingapp-7af2a.appspot.com',
      messagingSenderId: '1084074487224'
    }),
    AngularFireDatabaseModule,
    RouterModule.forRoot(routes)
  ],
  providers: [CommonService, DatePipe, UserService],
  bootstrap: [AppComponent],
  entryComponents: [BookingInformationComponent, RejectRequestComponent, InformationMessageComponent]
})
export class AppModule {}
