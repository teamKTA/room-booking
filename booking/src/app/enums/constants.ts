export const RequestStatus = {
  Pending: 'Pending',
  Approved: 'Approved',
  Rejected: 'Rejected'
};

export const Role = {
  Admin: 'admin',
  User: 'user',
  Moderator: 'mod'
};

