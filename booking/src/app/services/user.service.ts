import { Injectable } from '@angular/core';
import { FileBaseService } from './firebase.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { User } from '../models/user';

@Injectable()
export class UserService extends FileBaseService {
  collectionName = 'Users';
  public user: User;
  constructor(public db: AngularFireDatabase) {
    super(db);
    this.itemRef = this.db.list(this.collectionName);
  }


}
