import { Injectable } from '@angular/core';
import { FileBaseService } from './firebase.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class RequestService extends FileBaseService {
  collectionName = 'Requests';
  constructor(public db: AngularFireDatabase) {
    super(db);
    this.itemRef = this.db.list(this.collectionName);
  }


}
