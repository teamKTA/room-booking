import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable()
export class CommonService {
  constructor(private datePipe: DatePipe) {}
  /**
   * compare Two Object
   * @param object1 : object 1
   * @param object2  : object 2
   * @returns true if 2 objects are the same else return false
   */
  compareTwoObject(object1: any, object2: any): boolean {
    if (JSON.stringify(object1) !== JSON.stringify(object2)) {
      return false;
    }
    return true;
  }

  /**
   * return object if existed
   * @param listOptions : list finding
   * @param item : the item is finding in list
   * @returns object or null
   */
  recordExisted(list: any, item: any): any {
    return list.find(x => this.compareTwoObject(x, item));
  }

  transformDate(date: Date) {
    return this.datePipe.transform(date, 'dd-MM-yyyy');
  }
}
