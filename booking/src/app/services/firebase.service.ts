import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

@Injectable()
export class FileBaseService {
  public itemRef: AngularFireList<any>;

  constructor(public db: AngularFireDatabase) {}

  addItem(obj: any, key: string) {
    this.itemRef.set(key, obj);
  }

  updateItem(item: any, key: string) {
     this.itemRef.update(key, item);
  }

  deleteItem(key: string) {
    // this.books.remove(key);
  }

  deleteEverything() {
    // this.books.remove();
  }
}
