import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { MAT_DIALOG_DATA } from '@angular/material';
import { User } from '../../models/user';

@Component({
  selector: 'app-booking-information',
  templateUrl: 'booking-information.component.html',
  styleUrls: ['../../app.component.scss']
})
export class BookingInformationComponent implements OnInit, OnDestroy {
  bookingInformation: FormGroup;
  personNameFC = new FormControl('', Validators.required);
  purposeFC = new FormControl('', Validators.required);
  descriptionFC = new FormControl('', Validators.required);
  information: any;
  curretUser: User;

  constructor(
    public dialogRef: MatDialogRef<BookingInformationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnDestroy() {
    this.bookingInformation = new FormGroup({});
    this.personNameFC = new FormControl('', Validators.required);
    this.purposeFC = new FormControl('', Validators.required);
    this.descriptionFC = new FormControl('', Validators.required);
  }

  ngOnInit() {
    this.information = this.data.information;
    this.bookingInformation = new FormGroup({});
    this.addFormControls();
  }

  addFormControls() {
    this.bookingInformation.addControl('personName', this.personNameFC);
    this.bookingInformation.addControl('purpose', this.purposeFC);
    this.bookingInformation.addControl('description', this.descriptionFC);
  }

  onClickCancel() {
    this.dialogRef.close();
  }

  onClickOk() {
    const res = Object.assign(this.bookingInformation.value);
    this.dialogRef.close(res);
  }
}
