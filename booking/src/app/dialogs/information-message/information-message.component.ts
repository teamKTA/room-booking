import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-information-message',
  templateUrl: 'information-message.component.html',
  styleUrls: ['../../app.component.scss']
})
export class InformationMessageComponent implements OnInit, OnDestroy {
  information = '';
  constructor(
    public dialogRef: MatDialogRef<InformationMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnDestroy() {}
  ngOnInit() {
    this.information = this.data.information;
  }
}
