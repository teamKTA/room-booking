import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reject-request',
  templateUrl: 'reject-request.component.html',
  styleUrls: ['../../app.component.scss']
})
export class RejectRequestComponent implements OnInit, OnDestroy {
  descriptionFC = new FormControl('', Validators.required);
  constructor(public dialogRef: MatDialogRef<RejectRequestComponent>) {}
  ngOnDestroy() {}
  ngOnInit() {}

  onClickCancel() {
    this.dialogRef.close();
  }

  onClickOk() {
    if (this.descriptionFC.invalid === true) {
      return;
    }
    this.dialogRef.close(this.descriptionFC.value);
  }
}
